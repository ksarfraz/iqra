-- MySQL dump 10.13  Distrib 5.6.35, for macos10.12 (x86_64)
--
-- Host: localhost    Database: iqra_db
-- ------------------------------------------------------
-- Server version	5.6.35

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city`
--

LOCK TABLES `city` WRITE;
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
/*!40000 ALTER TABLE `city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `postviews`
--

DROP TABLE IF EXISTS `postviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `postviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_views` varchar(200) DEFAULT NULL,
  `post_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `view_status` bit(1) NOT NULL DEFAULT b'1',
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK7qf7rqs1deytqm5j8bx4oh5ad` (`post_id`),
  KEY `FK9yyymva010xgux61dadfpl1tr` (`user_id`),
  CONSTRAINT `FK7qf7rqs1deytqm5j8bx4oh5ad` FOREIGN KEY (`post_id`) REFERENCES `userpostuploads` (`id`),
  CONSTRAINT `FK9yyymva010xgux61dadfpl1tr` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `postviews`
--

LOCK TABLES `postviews` WRITE;
/*!40000 ALTER TABLE `postviews` DISABLE KEYS */;
INSERT INTO `postviews` VALUES (1,'I like the idea',1,'2021-03-07 17:24:05','2021-03-07 17:24:05','',2),(2,'We connect & discuss below two points as well',1,'2021-03-07 17:24:39','2021-03-07 17:24:39','',2);
/*!40000 ALTER TABLE `postviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'ROLE_ADMIN'),(2,'ROLE_MODERATOR'),(3,'ROLE_USER');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `user_id` bigint(20) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `FKh8ciramu9cc9q3qcqiv4ue8a6` (`role_id`),
  CONSTRAINT `FKh8ciramu9cc9q3qcqiv4ue8a6` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `FKhfh9dx7w3ubf1co1vdev94g3f` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` VALUES (2,2),(3,2),(2,3),(3,3);
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userpostuploads`
--

DROP TABLE IF EXISTS `userpostuploads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userpostuploads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_description` varchar(200) DEFAULT NULL,
  `post_path` varchar(70) DEFAULT NULL,
  `post_title` varchar(100) DEFAULT NULL,
  `post_type` varchar(20) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `post_status` bit(1) NOT NULL DEFAULT b'1',
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK3en4qm8rj2bf5ktyk2n73lu6k` (`user_id`),
  CONSTRAINT `FK3en4qm8rj2bf5ktyk2n73lu6k` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userpostuploads`
--

LOCK TABLES `userpostuploads` WRITE;
/*!40000 ALTER TABLE `userpostuploads` DISABLE KEYS */;
INSERT INTO `userpostuploads` VALUES (1,'This Post is realy nice','path','Post Title','prem',2,'2021-03-07 13:35:57','','2021-03-07 13:35:57'),(2,'This Post is realy nice more than first one','path-2','Post Title-2','prem-2',2,'2021-03-07 13:39:15','','2021-03-07 13:39:15');
/*!40000 ALTER TABLE `userpostuploads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `city` varchar(50) DEFAULT NULL,
  `contribution` varchar(10) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `current_ocupation` varchar(20) DEFAULT NULL,
  `education_qalification` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `fullname` varchar(100) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `geocordinates` varchar(50) DEFAULT NULL,
  `google_address` varchar(200) DEFAULT NULL,
  `locality` varchar(100) DEFAULT NULL,
  `mobile_number` varchar(50) DEFAULT NULL,
  `otp` varchar(10) DEFAULT NULL,
  `password` varchar(120) DEFAULT NULL,
  `physical_address` varchar(200) DEFAULT NULL,
  `reason_for_joining` varchar(200) DEFAULT NULL,
  `religion` varchar(10) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `status` bit(1) NOT NULL,
  `updated_at` datetime NOT NULL,
  `username` varchar(20) DEFAULT NULL,
  `user_active_status` bit(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKr43af9ap4edm43mmtq01oddj6` (`username`),
  UNIQUE KEY `UK6dotkott2kjsp8vw4d0m25fb7` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (2,'Mumbai','Time','India','2021-03-06 18:55:06','TPM','B-Tech','sksarfraz@gmail.com','SAEFRAZ Khan','M','12.99999,23.456','Sanjay Colony sector 23 google','Sanjay colony','9811172193','9650','$2a$10$FLXdnxVxExVUGWqwO2AZ5ecEWJcYT6AdQrH1K67CT.07D0pdC0tz6','Sanjay Colony sector 23','Personal Interest','Muslim','Haryana','\0','2021-03-06 18:55:06','sarfraz76','\0'),(3,'Faridabad','Time','India','2021-03-06 22:20:09','TPM','B-Tech','skprem@gmail.com','Prem Khan','M','12.99999,23.456','Sanjay Colony sector 23 google','Sanjay colony','9811172193',NULL,'$2a$10$OLSu4WcwWss.wq5gCuNsCucf/FGagYEJ6Wy06LrAQu46FE9GnPbq.','Sanjay Colony sector 23','Personal Interest','Muslim','Haryana','\0','2021-03-06 22:20:09','prem76','\0');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-09 18:06:11
