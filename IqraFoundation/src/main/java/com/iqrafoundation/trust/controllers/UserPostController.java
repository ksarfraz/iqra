package com.iqrafoundation.trust.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.iqrafoundation.trust.exception.ResourceNotFoundException;
import com.iqrafoundation.trust.models.Postviews;
import com.iqrafoundation.trust.models.User;
import com.iqrafoundation.trust.models.UserPostUpload;
import com.iqrafoundation.trust.payload.request.CreatePostRequest;
import com.iqrafoundation.trust.payload.request.CreateViewsRequest;
import com.iqrafoundation.trust.payload.response.MessageResponse;
import com.iqrafoundation.trust.repository.PostRepository;
import com.iqrafoundation.trust.repository.UserDetailRepository;
import com.iqrafoundation.trust.repository.ViewRepository;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/posts")
public class UserPostController {

	@Autowired
	PostRepository postRepository;

	@Autowired
	UserDetailRepository userDetailRepository;

	@Autowired
	ViewRepository viewRepository;

	// Create or upload the posts
	@PostMapping("/createpost/{id}")
	public ResponseEntity<MessageResponse> createPost(@PathVariable(value = "id") Long userId,
			@Valid @RequestBody CreatePostRequest createPostRequest) {

		User user = userDetailRepository.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException("User", "id", userId));

		UserPostUpload userPostUpload = new UserPostUpload(createPostRequest.getPostType(),
				createPostRequest.getPostTitle(), createPostRequest.getPostDescription(),
				createPostRequest.getPostPath());

		userPostUpload.setPostStatus(true);
		userPostUpload.setCreatedAt(new Date());
		userPostUpload.setUpdatedAt(new Date());
		userPostUpload.setUsers(user);
		postRepository.save(userPostUpload);
		return ResponseEntity.ok(new MessageResponse("Post updated successfully!"));
	}

	// Get All Active Posts
	@GetMapping("/getallposts")
	public List<UserPostUpload> getAllActivePosts() {
		// System.out.println("posts-->"+postRepository.findAllActivePosts(true).toString());
		List<UserPostUpload> postWithViews=new ArrayList<UserPostUpload>();
		List<UserPostUpload> userPosts=postRepository.findAllActivePosts(true);
		System.out.println(userPosts.size());
		for(UserPostUpload post:userPosts) {
			post.setPostviews(viewRepository.findAllActiveViewsOfSpecificPost(post.getId()));
			System.out.println(viewRepository.findAllActiveViewsOfSpecificPost(post.getId()).size());
			postWithViews.add(post);
		}		
		return postWithViews;
	}

	// Create or add your views about the post
	@PostMapping("/createview/{userId}/{postId}")
	public ResponseEntity<MessageResponse> addViewsAboutPost(@PathVariable(value = "userId") Long userId,
			@PathVariable(value = "postId") Long postId, @Valid @RequestBody CreateViewsRequest createViewsRequest) {

		User user = userDetailRepository.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException("User", "id", userId));

		UserPostUpload userPostUpload = postRepository.findById(postId)
				.orElseThrow(() -> new ResourceNotFoundException("User", "id", postId));

		Postviews postViews = new Postviews(createViewsRequest.getPostViews());

		postViews.setViewStatus(true);
		postViews.setCreatedAt(new Date());
		postViews.setUpdatedAt(new Date());
		postViews.setUsers(user);
		postViews.setUserpostuploads(userPostUpload);
		viewRepository.save(postViews);
		return ResponseEntity.ok(new MessageResponse("Post updated successfully!"));
	}

	// Get All Active comments or views
	@GetMapping("/getallviews")
	public List<Postviews> getAllActiveViews() {
		return viewRepository.findAll();
	}

}
