package com.iqrafoundation.trust.controllers;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.iqrafoundation.trust.exception.ResourceNotFoundException;
import com.iqrafoundation.trust.models.ERole;
import com.iqrafoundation.trust.models.Role;
import com.iqrafoundation.trust.models.User;
import com.iqrafoundation.trust.payload.request.ChangePassRequest;
import com.iqrafoundation.trust.payload.request.LoginRequest;
import com.iqrafoundation.trust.payload.request.SignupRequest;
import com.iqrafoundation.trust.payload.response.JwtResponse;
import com.iqrafoundation.trust.payload.response.MessageResponse;
import com.iqrafoundation.trust.repository.RoleRepository;
import com.iqrafoundation.trust.repository.UserDetailRepository;
import com.iqrafoundation.trust.repository.UserRepository;
import com.iqrafoundation.trust.security.jwt.JwtUtils;
import com.iqrafoundation.trust.security.services.UserDetailsImpl;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;

	@Autowired
	UserDetailRepository userDetailRepository;

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);

		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
		List<String> roles = userDetails.getAuthorities().stream().map(item -> item.getAuthority())
				.collect(Collectors.toList());

		return ResponseEntity.ok(new JwtResponse(userDetails.getFullname(), userDetails.getMobileNumber(),
				userDetails.getGender(), userDetails.getReligion(), userDetails.getEducationQalification(),
				userDetails.getCurrentOcupation(), userDetails.getReasonForJoining(), userDetails.getContribution(),
				userDetails.getPhysicalAddress(), userDetails.getGoogleAddress(), userDetails.getGeocordinates(),
				userDetails.getCountry(), userDetails.getState(), userDetails.getCity(), userDetails.getLocality(), jwt,
				userDetails.getId(), userDetails.getUsername(), userDetails.getEmail(), roles));
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return ResponseEntity.badRequest().body(new MessageResponse("Error: Username is already taken!"));
		}

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return ResponseEntity.badRequest().body(new MessageResponse("Error: Email is already in use!"));
		}

		// Create new user's account
		User user = new User(signUpRequest.getFullname(), signUpRequest.getMobileNumber(), signUpRequest.getGender(),
				signUpRequest.getReligion(), signUpRequest.getEducationQalification(),
				signUpRequest.getCurrentOcupation(), signUpRequest.getReasonForJoining(),
				signUpRequest.getContribution(), signUpRequest.getPhysicalAddress(), signUpRequest.getGoogleAddress(),
				signUpRequest.getGeocordinates(), signUpRequest.getCountry(), signUpRequest.getState(),
				signUpRequest.getCity(), signUpRequest.getLocality(), signUpRequest.getUsername(),
				signUpRequest.getEmail(), encoder.encode(signUpRequest.getPassword()));

		Set<String> strRoles = signUpRequest.getRole();
		Set<Role> roles = new HashSet<>();

		if (strRoles == null) {
			Role userRole = roleRepository.findByName(ERole.ROLE_USER)
					.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
			roles.add(userRole);
		} else {
			strRoles.forEach(role -> {
				switch (role) {
				case "admin":
					Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(adminRole);

					break;
				case "mod":
					Role modRole = roleRepository.findByName(ERole.ROLE_MODERATOR)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(modRole);

					break;
				default:
					Role userRole = roleRepository.findByName(ERole.ROLE_USER)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(userRole);
				}
			});
		}
		user.setRoles(roles);
		user.setUserActiveStatus(true);
		user.setStatus(true);
		user.setCreatedAt(new Date());
		user.setUpdatedAt(new Date());
		userRepository.save(user);
		return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
	}

	
	// Forgot Password for the logged in user
	@PutMapping("/forgotpass/{username}")
	public ResponseEntity<MessageResponse> forgotPass(@PathVariable(value = "username") String username,
			@Valid @RequestBody ChangePassRequest changePassRequest) {
		if (!(changePassRequest.getConfirmPass().equalsIgnoreCase(changePassRequest.getNewPass()))) {
			return ResponseEntity.ok(new MessageResponse("New Password & Confirm Password are not matching!"));
		}
		User user = userRepository.findByUsername(username)
				.orElseThrow(() -> new ResourceNotFoundException("User", "id", username));
		if (!(user.getOtp().equalsIgnoreCase(changePassRequest.getOtp()))) {
			return ResponseEntity.ok(new MessageResponse("Entered OTP is not matching !"));
		}

		user.setPassword(encoder.encode(changePassRequest.getNewPass()));
		userDetailRepository.save(user);
		return ResponseEntity.ok(new MessageResponse("Password changed successfully!"));
	}

	

	// Change Password for the logged in user
	@PutMapping("/passChange/{id}")
	public ResponseEntity<MessageResponse> passChange(@PathVariable(value = "id") Long userId,
			@Valid @RequestBody ChangePassRequest changePassRequest) {
		if (!(changePassRequest.getConfirmPass().equalsIgnoreCase(changePassRequest.getNewPass()))) {
			return ResponseEntity.ok(new MessageResponse("New Password & Confirm Password are not matching!"));
		}
		User user = userDetailRepository.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException("User", "id", userId));

		user.setPassword(encoder.encode(changePassRequest.getNewPass()));
		userDetailRepository.save(user);
		return ResponseEntity.ok(new MessageResponse("Password changed successfully!"));
	}

}
