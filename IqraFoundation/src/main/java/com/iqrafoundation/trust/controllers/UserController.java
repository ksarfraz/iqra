package com.iqrafoundation.trust.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.iqrafoundation.trust.exception.ResourceNotFoundException;
import com.iqrafoundation.trust.models.User;
import com.iqrafoundation.trust.repository.UserDetailRepository;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/users")
public class UserController {
	
	
	@Autowired
	UserDetailRepository userDetailRepository;
	
	// Get All Users
	@GetMapping("/getallusers")
	public List<User> getAllUsers() {
	    return userDetailRepository.findAll();
	}
	
	// Get user details from user id.
	@GetMapping("/getuserdetail/{id}")
	public 	User getUserById(@PathVariable(value = "id") Long userId) {
	    return userDetailRepository.findById(userId)
	            .orElseThrow(() -> new ResourceNotFoundException("User", "id", userId));
	}
	
	
	
	// Deactivate the user
	@PutMapping("/disableuser/{id}")
	public ResponseEntity<?> deleteNote(@PathVariable(value = "id") Long userId) {
		User user = userDetailRepository.findById(userId)
	            .orElseThrow(() -> new ResourceNotFoundException("User", "id", userId));

		user.setUserActiveStatus(false);
	    userDetailRepository.save(user);
	    return ResponseEntity.ok().build();
	}
	
	
}
