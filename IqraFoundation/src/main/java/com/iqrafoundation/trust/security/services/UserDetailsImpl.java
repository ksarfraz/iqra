package com.iqrafoundation.trust.security.services;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.iqrafoundation.trust.models.User;

public class UserDetailsImpl implements UserDetails {
	private static final long serialVersionUID = 1L;

	private Long id;

	private String username;

	private String email;
	private String fullname;
    private String mobileNumber;
	private String gender;
	private String religion;
	private String educationQalification;
	private String currentOcupation;
	private String reasonForJoining;
	private String contribution;
	private String physicalAddress;
	private String googleAddress;
	private String geocordinates;
	private String country;
	private String state;
	private String city;
	private String locality;

	@JsonIgnore
	private String password;

	private Collection<? extends GrantedAuthority> authorities;

	public UserDetailsImpl(String fullname,String mobileNumber,String gender,String religion,String educationQalification,String currentOcupation,String reasonForJoining,String contribution,String physicalAddress,String googleAddress,String geocordinates,String country,String state,String city,String locality,
			Long id, String username, String email, String password,
			Collection<? extends GrantedAuthority> authorities) {
		
		 this.fullname = fullname;
		 this.mobileNumber = mobileNumber;
		 this.gender = gender;
		 this.religion = religion;
		 this.educationQalification = educationQalification;
		 this.currentOcupation = currentOcupation;
		 this.reasonForJoining = reasonForJoining;
		 this.contribution = contribution;
		 this.physicalAddress = physicalAddress;
		 this.googleAddress = googleAddress;
		 this.geocordinates = geocordinates;
		 this.country = country;
		 this.state = state;
		 this.city = city;
		 this.locality = locality;
		this.id = id;
		this.username = username;
		this.email = email;
		this.password = password;
		this.authorities = authorities;
	}

	public static UserDetailsImpl build(User user) {
		List<GrantedAuthority> authorities = user.getRoles().stream()
				.map(role -> new SimpleGrantedAuthority(role.getName().name()))
				.collect(Collectors.toList());

		return new UserDetailsImpl(
				user.getFullname(),
				user.getMobileNumber(),
				user.getGender(),
				user.getReligion(),
				user.getEducationQalification(),
				user.getCurrentOcupation(),
				user.getReasonForJoining(),
				user.getContribution(),
				user.getPhysicalAddress(),
				user.getGoogleAddress(),
				user.getGeocordinates(),
				user.getCountry(),
				user.getState(),
				user.getCity(),
				user.getLocality(),
				user.getId(), 
				user.getUsername(), 
				user.getEmail(),
				user.getPassword(), 
				authorities);
	}

	
	
	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public String getEducationQalification() {
		return educationQalification;
	}

	public void setEducationQalification(String educationQalification) {
		this.educationQalification = educationQalification;
	}

	public String getCurrentOcupation() {
		return currentOcupation;
	}

	public void setCurrentOcupation(String currentOcupation) {
		this.currentOcupation = currentOcupation;
	}

	public String getReasonForJoining() {
		return reasonForJoining;
	}

	public void setReasonForJoining(String reasonForJoining) {
		this.reasonForJoining = reasonForJoining;
	}

	public String getContribution() {
		return contribution;
	}

	public void setContribution(String contribution) {
		this.contribution = contribution;
	}

	public String getPhysicalAddress() {
		return physicalAddress;
	}

	public void setPhysicalAddress(String physicalAddress) {
		this.physicalAddress = physicalAddress;
	}

	public String getGoogleAddress() {
		return googleAddress;
	}

	public void setGoogleAddress(String googleAddress) {
		this.googleAddress = googleAddress;
	}

	public String getGeocordinates() {
		return geocordinates;
	}

	public void setGeocordinates(String geocordinates) {
		this.geocordinates = geocordinates;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
		this.authorities = authorities;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	public Long getId() {
		return id;
	}

	public String getEmail() {
		return email;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		UserDetailsImpl user = (UserDetailsImpl) o;
		return Objects.equals(id, user.id);
	}
}
