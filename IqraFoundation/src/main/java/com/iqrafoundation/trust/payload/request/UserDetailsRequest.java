package com.iqrafoundation.trust.payload.request;

import java.util.Set;

public class UserDetailsRequest {

	
    private Set<String> role;

	public Set<String> getRole() {
		return role;
	}

	public void setRole(Set<String> role) {
		this.role = role;
	}

	
	
}
