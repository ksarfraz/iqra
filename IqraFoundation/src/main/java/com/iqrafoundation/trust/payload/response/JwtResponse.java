package com.iqrafoundation.trust.payload.response;

import java.util.List;

public class JwtResponse {	
	private String fullname;
    private String mobileNumber;
	private String gender;
	private String religion;
	private String educationQalification;
	private String currentOcupation;
	private String reasonForJoining;
	private String contribution;
	private String physicalAddress;
	private String googleAddress;
	private String geocordinates;
	private String country;
	private String state;
	private String city;
	private String locality;
	private String token;
	private String type = "Bearer";
	private Long id;
	private String username;
	private String email;
	private List<String> roles;

	public JwtResponse(String fullname,String mobileNumber,String gender,String religion,String educationQalification,String currentOcupation,String reasonForJoining,String contribution,String physicalAddress,String googleAddress,String geocordinates,String country,String state,String city,String locality,
			String accessToken, Long id, String username, String email, List<String> roles) {		
		 this.fullname = fullname;
		 this.mobileNumber = mobileNumber;
		 this.gender = gender;
		 this.religion = religion;
		 this.educationQalification = educationQalification;
		 this.currentOcupation = currentOcupation;
		 this.reasonForJoining = reasonForJoining;
		 this.contribution = contribution;
		 this.physicalAddress = physicalAddress;
		 this.googleAddress = googleAddress;
		 this.geocordinates = geocordinates;
		 this.country = country;
		 this.state = state;
		 this.city = city;
		 this.locality = locality;
		 this.token = accessToken;
		 this.id = id;
		 this.username = username;
		 this.email = email;
		 this.roles = roles;
	}
	
	

	public String getFullname() {
		return fullname;
	}



	public void setFullname(String fullname) {
		this.fullname = fullname;
	}



	public String getMobileNumber() {
		return mobileNumber;
	}



	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}



	public String getGender() {
		return gender;
	}



	public void setGender(String gender) {
		this.gender = gender;
	}



	public String getReligion() {
		return religion;
	}



	public void setReligion(String religion) {
		this.religion = religion;
	}



	public String getEducationQalification() {
		return educationQalification;
	}



	public void setEducationQalification(String educationQalification) {
		this.educationQalification = educationQalification;
	}



	public String getCurrentOcupation() {
		return currentOcupation;
	}



	public void setCurrentOcupation(String currentOcupation) {
		this.currentOcupation = currentOcupation;
	}



	public String getReasonForJoining() {
		return reasonForJoining;
	}



	public void setReasonForJoining(String reasonForJoining) {
		this.reasonForJoining = reasonForJoining;
	}



	public String getContribution() {
		return contribution;
	}



	public void setContribution(String contribution) {
		this.contribution = contribution;
	}



	public String getPhysicalAddress() {
		return physicalAddress;
	}



	public void setPhysicalAddress(String physicalAddress) {
		this.physicalAddress = physicalAddress;
	}



	public String getGoogleAddress() {
		return googleAddress;
	}



	public void setGoogleAddress(String googleAddress) {
		this.googleAddress = googleAddress;
	}



	public String getGeocordinates() {
		return geocordinates;
	}



	public void setGeocordinates(String geocordinates) {
		this.geocordinates = geocordinates;
	}



	public String getCountry() {
		return country;
	}



	public void setCountry(String country) {
		this.country = country;
	}



	public String getState() {
		return state;
	}



	public void setState(String state) {
		this.state = state;
	}



	public String getCity() {
		return city;
	}



	public void setCity(String city) {
		this.city = city;
	}



	public String getLocality() {
		return locality;
	}



	public void setLocality(String locality) {
		this.locality = locality;
	}



	public String getToken() {
		return token;
	}



	public void setToken(String token) {
		this.token = token;
	}



	public String getType() {
		return type;
	}



	public void setType(String type) {
		this.type = type;
	}



	public void setRoles(List<String> roles) {
		this.roles = roles;
	}



	public String getAccessToken() {
		return token;
	}

	public void setAccessToken(String accessToken) {
		this.token = accessToken;
	}

	public String getTokenType() {
		return type;
	}

	public void setTokenType(String tokenType) {
		this.type = tokenType;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<String> getRoles() {
		return roles;
	}
}
