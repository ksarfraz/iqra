package com.iqrafoundation.trust.payload.request;

import javax.validation.constraints.NotBlank;

public class ChangePassRequest {
	
	@NotBlank
	private String newPass;

	@NotBlank
	private String confirmPass;
	
	
	private String Otp;

	public String getNewPass() {
		return newPass;
	}

	public void setNewPass(String newPass) {
		this.newPass = newPass;
	}

	public String getConfirmPass() {
		return confirmPass;
	}

	public void setConfirmPass(String confirmPass) {
		this.confirmPass = confirmPass;
	}

	public String getOtp() {
		return Otp;
	}

	public void setOtp(String otp) {
		Otp = otp;
	}

	

}
