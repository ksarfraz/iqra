package com.iqrafoundation.trust.payload.request;

import javax.validation.constraints.NotBlank;

public class CreatePostRequest {	
	
	    //Audio,Video,Image,Text
		@NotBlank
		private String postType;

		@NotBlank
		private String postTitle;

		private String postDescription;
		
		private String postPath;

		public String getPostType() {
			return postType;
		}

		public void setPostType(String postType) {
			this.postType = postType;
		}

		public String getPostTitle() {
			return postTitle;
		}

		public void setPostTitle(String postTitle) {
			this.postTitle = postTitle;
		}

		public String getPostDescription() {
			return postDescription;
		}

		public void setPostDescription(String postDescription) {
			this.postDescription = postDescription;
		}

		public String getPostPath() {
			return postPath;
		}

		public void setPostPath(String postPath) {
			this.postPath = postPath;
		}
		
		

}
