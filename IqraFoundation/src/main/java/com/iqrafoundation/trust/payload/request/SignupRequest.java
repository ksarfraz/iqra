package com.iqrafoundation.trust.payload.request;

import java.util.Set;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
 
public class SignupRequest {
	
	@NotBlank
	@Size(max = 100)
	private String fullname;
	
	@NotBlank
	@Size(max = 50)
	private String mobileNumber;
	
	@NotBlank
	@Size(max = 10)
	private String gender;
	
	@NotBlank
	@Size(max = 10)
	private String religion;
	
	@NotBlank
	@Size(max = 20)
	private String educationQalification;
	
	@NotBlank
	@Size(max = 20)
	private String currentOcupation;
	
	@NotBlank
	@Size(max = 200)
	private String reasonForJoining;
	
	@NotBlank
	@Size(max = 10)
	private String contribution;
	
	@NotBlank
	@Size(max = 200)
	private String physicalAddress;
	
	@NotBlank
	@Size(max = 200)
	private String googleAddress;
	
	@NotBlank
	@Size(max = 50)
	private String geocordinates;
	
	@NotBlank
	@Size(max = 50)
	private String country;
	
	@NotBlank
	@Size(max = 50)
	private String state;
	
	@NotBlank
	@Size(max = 50)
	private String city;
	
	@NotBlank
	@Size(max = 100)
	private String locality;
	
    @NotBlank
    @Size(min = 3, max = 20)
    private String username;
 
    @NotBlank
    @Size(max = 50)
    @Email
    private String email;
    
    private Set<String> role;
    
    @NotBlank
    @Size(min = 6, max = 40)
    private String password;
  
    
    
    public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public String getEducationQalification() {
		return educationQalification;
	}

	public void setEducationQalification(String educationQalification) {
		this.educationQalification = educationQalification;
	}

	public String getCurrentOcupation() {
		return currentOcupation;
	}

	public void setCurrentOcupation(String currentOcupation) {
		this.currentOcupation = currentOcupation;
	}

	public String getReasonForJoining() {
		return reasonForJoining;
	}

	public void setReasonForJoining(String reasonForJoining) {
		this.reasonForJoining = reasonForJoining;
	}

	public String getContribution() {
		return contribution;
	}

	public void setContribution(String contribution) {
		this.contribution = contribution;
	}

	public String getPhysicalAddress() {
		return physicalAddress;
	}

	public void setPhysicalAddress(String physicalAddress) {
		this.physicalAddress = physicalAddress;
	}

	public String getGoogleAddress() {
		return googleAddress;
	}

	public void setGoogleAddress(String googleAddress) {
		this.googleAddress = googleAddress;
	}

	public String getGeocordinates() {
		return geocordinates;
	}

	public void setGeocordinates(String geocordinates) {
		this.geocordinates = geocordinates;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}

	public String getUsername() {
        return username;
    }
 
    public void setUsername(String username) {
        this.username = username;
    }
 
    public String getEmail() {
        return email;
    }
 
    public void setEmail(String email) {
        this.email = email;
    }
 
    public String getPassword() {
        return password;
    }
 
    public void setPassword(String password) {
        this.password = password;
    }
    
    public Set<String> getRole() {
      return this.role;
    }
    
    public void setRole(Set<String> role) {
      this.role = role;
    }
}
