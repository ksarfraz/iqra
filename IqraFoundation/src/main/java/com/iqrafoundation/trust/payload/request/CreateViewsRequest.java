package com.iqrafoundation.trust.payload.request;

import javax.validation.constraints.NotBlank;

public class CreateViewsRequest {	
	
	@NotBlank
	private String postViews;

	public String getPostViews() {
		return postViews;
	}

	public void setPostViews(String postViews) {
		this.postViews = postViews;
	}
		
		

}
