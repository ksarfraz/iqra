package com.iqrafoundation.trust.models;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.ColumnDefault;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

@Entity
@Table(	name = "users", 
		uniqueConstraints = { 
			@UniqueConstraint(columnNames = "username"),
			@UniqueConstraint(columnNames = "email") 
		})
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotBlank
	@Size(max = 100)
	private String fullname;
	
	@NotBlank
	@Size(max = 50)
	private String mobileNumber;
	
	@NotBlank
	@Size(max = 10)
	private String gender;
	
	@NotBlank
	@Size(max = 10)
	private String religion;
	
	@NotBlank
	@Size(max = 20)
	private String educationQalification;
	
	@NotBlank
	@Size(max = 20)
	private String currentOcupation;
	
	@NotBlank
	@Size(max = 200)
	private String reasonForJoining;
	
	@NotBlank
	@Size(max = 10)
	private String contribution;
	
	@NotBlank
	@Size(max = 200)
	private String physicalAddress;
	
	@NotBlank
	@Size(max = 200)
	private String googleAddress;
	
	@NotBlank
	@Size(max = 50)
	private String geocordinates;
	
	@NotBlank
	@Size(max = 50)
	private String country;
	
	@NotBlank
	@Size(max = 50)
	private String state;
	
	@NotBlank
	@Size(max = 50)
	private String city;
	
	@NotBlank
	@Size(max = 100)
	private String locality;
	
	@NotBlank
	@Size(max = 20)
	private String username;

	@NotBlank
	@Size(max = 50)
	private String email;
	
	
    @Column(name = "user_active_status", nullable = false)
	private boolean userActiveStatus;
    
	@NotBlank
	@Size(max = 120)
	private String password;
	
	@ColumnDefault(value = "false")
	private boolean status;


	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at", nullable = false, updatable = false)
    @CreatedDate
    private Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_at", nullable = false)
    @LastModifiedDate
    private Date updatedAt;
    
	@Size(max = 10)
	private String otp;


	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(	name = "user_roles", 
				joinColumns = @JoinColumn(name = "user_id"), 
				inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<Role> roles = new HashSet<>();
	
	@OneToMany(mappedBy = "users", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private Set<UserPostUpload> userpostuploads;
	
	@OneToMany(mappedBy = "users", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private Set<Postviews> postviews;

	public User() {
	}

	
	public User(String fullname,String mobileNumber,String gender,String religion,String educationQalification,String currentOcupation,String reasonForJoining,String contribution,String physicalAddress,String googleAddress,String geocordinates,String country,String state,String city,String locality,
			String username, String email, String password) {
		 this.fullname = fullname;
		 this.mobileNumber = mobileNumber;
		 this.gender = gender;
		 this.religion = religion;
		 this.educationQalification = educationQalification;
		 this.currentOcupation = currentOcupation;
		 this.reasonForJoining = reasonForJoining;
		 this.contribution = contribution;
		 this.physicalAddress = physicalAddress;
		 this.googleAddress = googleAddress;
		 this.geocordinates = geocordinates;
		 this.country = country;
		 this.state = state;
		 this.city = city;
		 this.locality = locality;
	     this.username = username;
	     this.email = email;
	     this.password = password;
}
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public String getEducationQalification() {
		return educationQalification;
	}

	public void setEducationQalification(String educationQalification) {
		this.educationQalification = educationQalification;
	}

	public String getCurrentOcupation() {
		return currentOcupation;
	}

	public void setCurrentOcupation(String currentOcupation) {
		this.currentOcupation = currentOcupation;
	}

	public String getReasonForJoining() {
		return reasonForJoining;
	}

	public void setReasonForJoining(String reasonForJoining) {
		this.reasonForJoining = reasonForJoining;
	}

	public String getContribution() {
		return contribution;
	}

	public void setContribution(String contribution) {
		this.contribution = contribution;
	}

	public String getPhysicalAddress() {
		return physicalAddress;
	}

	public void setPhysicalAddress(String physicalAddress) {
		this.physicalAddress = physicalAddress;
	}

	public String getGoogleAddress() {
		return googleAddress;
	}

	public void setGoogleAddress(String googleAddress) {
		this.googleAddress = googleAddress;
	}

	public String getGeocordinates() {
		return geocordinates;
	}

	public void setGeocordinates(String geocordinates) {
		this.geocordinates = geocordinates;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}
	
	
	public boolean isUserActiveStatus() {
		return userActiveStatus;
	}


	public void setUserActiveStatus(boolean userActiveStatus) {
		this.userActiveStatus = userActiveStatus;
	}


	public Date getCreatedAt() {
		return createdAt;
	}


	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}


	public Date getUpdatedAt() {
		return updatedAt;
	}


	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}


	public String getOtp() {
		return otp;
	}


	public void setOtp(String otp) {
		this.otp = otp;
	}


	public boolean isStatus() {
		return status;
	}


	public void setStatus(boolean status) {
		this.status = status;
	}


	public Set<UserPostUpload> getUserpostuploads() {
		return userpostuploads;
	}


	public void setUserpostuploads(Set<UserPostUpload> userpostuploads) {
		this.userpostuploads = userpostuploads;
	}


	public Set<Postviews> getPostviews() {
		return postviews;
	}


	public void setPostviews(Set<Postviews> postviews) {
		this.postviews = postviews;
	}
	
	
}
