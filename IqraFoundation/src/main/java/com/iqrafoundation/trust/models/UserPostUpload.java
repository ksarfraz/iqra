package com.iqrafoundation.trust.models;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.ColumnDefault;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "userpostuploads")
public class UserPostUpload implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	//Audio,Video,Image,Text
	@NotBlank
	@Size(max = 20)
	private String postType;

	@NotBlank
	@Size(max = 100)
	private String postTitle;

	@Size(max = 200)
	private String postDescription;
	
	@Size(max = 70)
	private String postPath;
	
	@ColumnDefault(value = "true")
	private boolean postStatus;

	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at", nullable = false, updatable = false)
    @CreatedDate
    private Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_at", nullable = false)
    @LastModifiedDate
    private Date updatedAt;
	
	@OneToMany(mappedBy = "userpostuploads", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private Set<Postviews> postviews;
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    private User users;
	

	public UserPostUpload() {

	}
	
	public UserPostUpload(String postType,String postTitle,String postDescription,String postPath) {
		 this.postType=postType;
		 this.postTitle = postTitle;
		 this.postDescription = postDescription;
	     this.postPath = postPath;
	}


	


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPostType() {
		return postType;
	}


	public void setPostType(String postType) {
		this.postType = postType;
	}


	public String getPostTitle() {
		return postTitle;
	}


	public void setPostTitle(String postTitle) {
		this.postTitle = postTitle;
	}


	public String getPostDescription() {
		return postDescription;
	}


	public void setPostDescription(String postDescription) {
		this.postDescription = postDescription;
	}


	public String getPostPath() {
		return postPath;
	}


	public void setPostPath(String postPath) {
		this.postPath = postPath;
	}


	public Set<Postviews> getPostviews() {
		return postviews;
	}


	public void setPostviews(Set<Postviews> postviews) {
		this.postviews = postviews;
	}


	public User getUsers() {
		return users;
	}


	public void setUsers(User users) {
		this.users = users;
	}

	public boolean isPostStatus() {
		return postStatus;
	}

	public void setPostStatus(boolean postStatus) {
		this.postStatus = postStatus;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}



	
}