package com.iqrafoundation.trust.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.iqrafoundation.trust.models.Postviews;

public interface ViewRepository extends JpaRepository<Postviews, Long> {
	
	  @Query(value = "select * FROM postviews pv JOIN userpostuploads u ON u.id = pv.post_id where pv.post_id = ?1",nativeQuery = true)
	   Set<Postviews> findAllActiveViewsOfSpecificPost(Long postId);


}
