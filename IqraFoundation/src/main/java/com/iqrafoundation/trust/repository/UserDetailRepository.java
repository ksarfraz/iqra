package com.iqrafoundation.trust.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.iqrafoundation.trust.models.User;

@Repository
public interface UserDetailRepository extends JpaRepository<User, Long> {
	

}
