package com.iqrafoundation.trust.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.iqrafoundation.trust.models.UserPostUpload;

@Repository
public interface PostRepository extends JpaRepository<UserPostUpload, Long> {

	
	  @Query(value = "select * FROM userpostuploads u JOIN postviews pv ON u.id = pv.post_id where u.post_status = ?1", nativeQuery = true)
	   List<UserPostUpload> findAllActivePosts(boolean status);

}
